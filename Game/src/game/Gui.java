package game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import json.JSONObject;
import utils.map.Tile;

public class Gui extends JFrame implements Runnable{
    //NOTE: Game based fields
    private static final JSONObject jGame = Game.jHnd.getJSONObject("game");
    private static int WIDTH = jGame.getJSONObject("gui").getInt("width");
    private static int HEIGHT = jGame.getJSONObject("gui").getInt("height");
    private static int HOR_TILES = jGame.getInt("hor-tiles");
    private static int VER_TILES = jGame.getInt("ver-tiles");
    
    //NOTE: Game constants that rely on loaded settings
    private static int HOR_MID = HOR_TILES / 2, VER_MID = VER_TILES / 2;
    
    //NOTE: Local instance fields
    private static ArrayList<Tile> view = new ArrayList<Tile>();
    private static Tile plyr = null;
    private static boolean resize = false;
    private static int offX = 0, offY = 0;
    private static boolean keyUp = false, keyDown = false, keyLeft = false, keyRight = false;
    
    public Gui(String title){ super(title); }
    
    @Override
    public void run(){
        setSize(WIDTH, HEIGHT);
        Tile.setScaleAll(WIDTH / VER_TILES, HEIGHT / HOR_TILES);
        
        getContentPane().add(new JPanel(){
            { setBackground(Color.BLACK); }
            
            @Override
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                
                int width = Gui.WIDTH;
                int height = Gui.HEIGHT;
                int horTiles = Gui.HOR_TILES;
                int verTiles = Gui.VER_TILES;
                int offX = Gui.offX;
                int offY = Gui.offY;
                if(resize){
                    Tile.setScaleAll(width / horTiles, height / verTiles);
                    resize = false;
                }
                view.stream().forEach((t) -> { if(t.getScaledImage()!=null)g.drawImage(t.getScaledImage(), (t.getX() * t.getScaleWidth()) + offX, (t.getY() * t.getScaleHeight()) + offY, this); });
                if(plyr!=null)g.drawImage(plyr.getScaledImage(), HOR_MID * plyr.getScaleWidth(), VER_MID * plyr.getScaleHeight(), this);
            }
        });
        
        getRootPane().addComponentListener(new ComponentAdapter(){
            @Override
            public void componentResized(ComponentEvent e){ resize(); }
        });
        
        setVisible(true);
        
        addKeyListener(new KeyListener(){
            @Override
            public void keyPressed(KeyEvent e){
                switch(e.getKeyCode()){
                    case KeyEvent.VK_W :
                    case KeyEvent.VK_UP : Gui.keyUp = true;
                        break;
                    case KeyEvent.VK_A :
                    case KeyEvent.VK_LEFT : Gui.keyLeft = true;
                        break;
                    case KeyEvent.VK_S :
                    case KeyEvent.VK_DOWN : Gui.keyDown = true;
                        break;
                    case KeyEvent.VK_D :
                    case KeyEvent.VK_RIGHT : Gui.keyRight = true;
                }
            }

            @Override
            public void keyReleased(KeyEvent e){
                switch(e.getKeyCode()){
                    case KeyEvent.VK_W :
                    case KeyEvent.VK_UP : Gui.keyUp = false;
                        break;
                    case KeyEvent.VK_A :
                    case KeyEvent.VK_LEFT : Gui.keyLeft = false;
                        break;
                    case KeyEvent.VK_S :
                    case KeyEvent.VK_DOWN : Gui.keyDown = false;
                        break;
                    case KeyEvent.VK_D :
                    case KeyEvent.VK_RIGHT : Gui.keyRight = false;
                }
            }

            @Override
            public void keyTyped(KeyEvent e){}
        });
    }
    
    public void display(float offX, float offY, ArrayList<Tile> view){
        this.offX = (int)(Tile.getScaleWidth() * offX);
        this.offY = (int)(Tile.getScaleHeight() * offY);
        this.view = view;
        repaint();
    }
    
    private void resize(){
        WIDTH = getWidth();
        HEIGHT = getHeight();
        resize = true;
        repaint();
    }
    
    public boolean getUp(){ return keyUp; }
    
    public boolean getDown(){ return keyDown; }
    
    public boolean getLeft(){ return keyLeft; }
    
    public boolean getRight(){ return keyRight; }
    
    public void setPlayer(Tile plyr){ this.plyr = plyr; }
}