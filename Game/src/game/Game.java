package game;

import java.awt.Image;
import java.util.ArrayList;
import javax.swing.SwingUtilities;
import json.JSONArray;
import json.JSONObject;
import utils.debug.Out;
import utils.map.Map;
import utils.map.Tile;
import utils.settings.JsonHandler;

public class Game{
    //NOTE: Game constants
    private static final long NANO = 1000000000, MICRO = 1000000, MILLI = 1000;
    public static final JsonHandler jHnd = new JsonHandler("settings.json");
    
    //NOTE: Game based fields
    private static final JSONObject jGame = jHnd.getJSONObject("game");
    private static long UPS = jGame.getLong("ups"), FRAME = NANO / UPS;
    private static boolean detectLag = jGame.getBoolean("detect-lag");
    private static int HOR_TILES = jGame.getInt("hor-tiles");
    private static int VER_TILES = jGame.getInt("ver-tiles");
    
    //NOTE: Game constants that rely on loaded settings
    private static final int HOR_MID = HOR_TILES / 2, VER_MID = VER_TILES / 2;
    
    //NOTE: Player based fields
    private static final JSONObject jPlayer = jHnd.getJSONObject("player");
    private static float moveSpeed = (float)jPlayer.getDouble("move-speed");
    private Tile pUp = new Tile(0, 0, jPlayer.getString("img-up"));
    private Tile pDown = new Tile(0, 0, jPlayer.getString("img-down"));
    private Tile pLeft = new Tile(0, 0,jPlayer.getString("img-left"));
    private Tile pRight = new Tile(0, 0, jPlayer.getString("img-right"));
    private float offX = (float)jPlayer.getDouble("start-x");
    private float offY = (float)jPlayer.getDouble("start-y");
    
    //NOTE: Map based fields
    private static final JSONObject jMap = jHnd.getJSONObject("map");
    private static final boolean procedural = jMap.getBoolean("procedural");
    
    //NOTE: Local instance fields
    private Gui gui;
    private Map map;
    
    public static void main(String[] args){ new Game(); }
    
    public Game(){
        //NOTE: Build fundamentals to game
        gui = new Gui(jGame.getJSONObject("gui").getString("title"));
        SwingUtilities.invokeLater(gui);
        if(procedural)map = new Map("world1", 100, 100);
        else map = new Map(jMap.getString("map-0"));
        gui.setPlayer(pDown);
        
        //NOTE: Get list of collideables
        ArrayList<Image> collideables = new ArrayList<Image>();
        JSONArray temp = jMap.getJSONArray("collideables");
        for(int i=0; i<temp.length(); i++)collideables.add(Tile.getImage((String)temp.get(i)));
        
        long target = 0;
        int diff = 0;
        while(true){
            //NOTE: Work out how much time is to be made up from last loop
            diff = (int)(target - System.nanoTime());
            //NOTE: Set new target time to complete loop in taking lag into consideration
            target = diff + System.nanoTime() + FRAME; //NOTE: Save time at start of method call
            
            //NOTE: Generate view and collideables
            ArrayList<Tile> view = new ArrayList<Tile>();
            ArrayList<Tile> detected = new ArrayList<Tile>();
            int lowMidHor = HOR_MID - 1;
            int highMidHor = HOR_MID + 1;
            int lowMidVer = VER_MID - 1;
            int highMidVer = VER_MID + 1;
            
            //NOTE: Check for important tiles in tile list
            map.map.stream().forEach((t) -> {
                if((t.getX()+offX)>-1       &&(t.getX()+offX)<HOR_TILES &&(t.getY()+offY)>-1       &&(t.getY()+offY)<VER_TILES) view.add(t);
                if((t.getX()+offX)>lowMidHor&&(t.getX()+offX)<highMidHor&&(t.getY()+offY)>lowMidVer&&(t.getY()+offY)<highMidVer)detected.add(t);
            });
            
            //NOTE: Check for collision
            boolean up = gui.getUp();
            boolean down = gui.getDown();
            boolean left = gui.getLeft();
            boolean right = gui.getRight();
            float tempOffY = offY + (up   ? moveSpeed : 0) - (down  ? moveSpeed : 0);
            float tempOffX = offX + (left ? moveSpeed : 0) - (right ? moveSpeed : 0);
            for(Tile t : detected){
                if(collideables.contains(t.getImage())){
                    //NOTE: Direct sides
                    if(t.getY()+tempOffY<VER_MID)up = false;
                    if(t.getY()+tempOffY>VER_MID)down = false;
                    if(t.getX()+tempOffX<HOR_MID)left = false;
                    if(t.getX()+tempOffX>HOR_MID)right = false;
                }
            }
            
            //NOTE: Do Movement
            if(left)offX += moveSpeed;
            if(right)offX -= moveSpeed;
            if(down)offY -= moveSpeed;
            if(up)offY += moveSpeed;
            
            //NOTE: Character sprite for direction
            if(left)gui.setPlayer(pLeft);
            else{
                if(right)gui.setPlayer(pRight);
                else{
                    if(down)gui.setPlayer(pDown);
                    else if(up)gui.setPlayer(pUp);
                }
            }
            
            //NOTE: Update display
            (new Thread(new Runnable(){
                @Override
                public void run(){ gui.display(offX, offY, view); }
            })).start();
            
            if(detectLag){
                //NOTE: Check to see if we are lagging
                if(target - System.nanoTime()<0){
                    //NOTE: Let user know how much lag there is
                    Out.status(Out.State.ERR, "Lag detected: " + (target - System.nanoTime()));
                }
            }
            
            //NOTE: Make Thread sleep until end of run
            while(true){
                //NOTE: How long to sleep in milli-seconds
                diff = (int)((target - System.nanoTime()) / MICRO);
                if(diff<=0)break;
                try{ Thread.sleep(diff); }
                catch(InterruptedException e){}
            }
        }
    }
}