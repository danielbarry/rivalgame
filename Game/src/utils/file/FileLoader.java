package utils.file;

import game.Game;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import utils.debug.Out;

public abstract class FileLoader{
    public static String getLocation(){ return new File(Game.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getPath(); }
    
    public static byte[] readFile(String fileName){
        Out.status(Out.State.DEBUG, "file name -> " + fileName);
        byte[] temp = new byte[0];
        try{
            InputStream is = Game.class.getResourceAsStream(fileName);
            Out.status(Out.State.DEBUG, "predicted file size -> " + is.available());
            
            int read = 0;
            int offset = 0;
            temp = new byte[is.available()];
            
            while(true){
                read = is.read(temp, offset, temp.length - offset);
                offset += read;
                if(read<0)break;
            }
        }catch(IOException e){ Out.status(Out.State.ERR, e.toString()); }
        Out.status(Out.State.DEBUG, "bytes read -> " + temp.length);
        return temp;
    }
}