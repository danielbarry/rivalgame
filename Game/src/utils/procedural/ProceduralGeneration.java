package utils.procedural;

import utils.debug.Out;

public abstract class ProceduralGeneration{
    
    public static int[][] generate(int[] key, int width, int height, int min, int max){
        //NOTE: Field initialization
        int top = max - min + 1;
        int[][] a = new int[height][width];
        
        //NOTE: Check to see if key field empt, if so fill with zero
        if(key.length==0)key = new int[]{0};
        
        //NOTE: First layer production
        a[0][0] = ((key[width % key.length] +
                   key[height % key.length] +
                   (int)Math.PI)
                % top) + min;
        for(int i=1; i<a[0].length; i++){
            a[0][i] = ((a[0][i-1] +
                        a[0][key[i % key.length] % a[0].length] +
                        key[(width + height) % key.length])
                    % top) + min;
        }
        
        //NOTE: Multi-layer production
        for(int b=1; b<a.length; b++){
            //NOTE: Initialize first in multi-layer
            a[b][0] = ((a[b-1][0] +
                        key[b % key.length] +
                        a[b][key[b % key.length] % a[b].length] +
                        a[0][b % a[b].length])
                    % top) + min;
            //NOTE: Build inner multilayer
            for(int c=1; c<a[b].length; c++){
                a[b][c] = ((a[b][c-1] +
                            a[b-1][c] +
                            a[b-1][c-1] +
                            (b * c) +
                            key[(c + b) % key.length])
                        % top) + min;
            }
        }
        return a;
    }
    
    public static void main(String[] args){
        Out.status(Out.State.DEBUG, "Testing " + ProceduralGeneration.class.getSimpleName());
        int high = 90;
        int low = 65;
        int[] test1 = new int[]{};
        int[][] result1 = generate(test1, 100, 100, low, high);
        for(int[] r : result1){
            char[] print = new char[r.length];
            for(int i=0; i<r.length; i++)print[i] = (char)r[i];
            Out.status(Out.State.DEBUG, new String(print));
        }
    }
}