package utils.map;

import game.Game;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import javax.imageio.ImageIO;
import utils.debug.Out;
import utils.procedural.ProceduralGeneration;

public class Tile{
    private static final ArrayList<WeakReference<Tile>> allTiles = new ArrayList<WeakReference<Tile>>();
    
    private static final HashMap<String, Image> images = new HashMap<String, Image>();
    private static HashMap<String, Image> scaledImages = new HashMap<String, Image>();
    
    private String name;
    private int x = 0, y = 0;
    private static int scaleWidth = 64, scaleHeight = 64;
    
    public Tile(int x, int y, String name){
        allTiles.add(new WeakReference(this));
        this.x = x;
        this.y = y;
        this.name = name;
        if(!images.containsKey(name)){
            URL url = Game.class.getResource("/res/tiles/" + name + ".bmp");
            if(url!=null){
                try{
                    Image i = ImageIO.read(url);
                    images.put(name, i);
                    scaledImages.put(name, (scaleWidth>0&&scaleHeight>0 ? i.getScaledInstance(scaleWidth, scaleHeight, Image.SCALE_SMOOTH) : i));
                }catch(IOException e){ Out.status(Out.State.ERR, e.toString()); }
            }else images.put(name, null);
        }
    }
    
    public Tile(int x, int y, Tile tile){
        allTiles.add(new WeakReference(this));
        this.x = x;
        this.y = y;
        this.name = tile.getName();
    }
    
    public Tile(int x, int y, String name, int[] key, int min, int max){
        allTiles.add(new WeakReference(this));
        this.x = x;
        this.y = y;
        this.name = name;
        if(!images.containsKey(name)){
            int[][] tempImg = ProceduralGeneration.generate(key, 32, 32, min, max);
            int[] img = new int[tempImg.length * tempImg[0].length];
            for(int i=0; i<tempImg.length; i++){
                System.arraycopy(tempImg[i], 0, img, i*tempImg[0].length, tempImg[0].length);
            }
            BufferedImage i = new BufferedImage(32, 32, BufferedImage.TYPE_BYTE_INDEXED);
            WritableRaster r = (WritableRaster)i.getData();
            r.setPixels(0, 0, 32, 32, img);
            i.setData(r);
            images.put(name, i);
            scaledImages.put(name, (scaleWidth>0&&scaleHeight>0 ? i.getScaledInstance(scaleWidth, scaleHeight, Image.SCALE_SMOOTH) : i));
        }
    }
    
    public String getName(){ return name; }
    
    public Image getImage(){ return images.get(name); }
    
    public static Image getImage(String name){ return images.get(name); }
    
    public Image getScaledImage(){ return scaledImages.get(name); }
    
    public static int getScaleWidth(){ return scaleWidth; }
    
    public static int getScaleHeight(){ return scaleHeight; }
    
    public int getX(){ return x; }
    
    public int getY(){ return y; }
    
    public static void setScaleAll(int width, int height){
        if(width>0&&height>0){
            scaleWidth = width;
            scaleHeight = height;
            scaledImages.clear();
            for(String s : images.keySet())scaledImages.put(s, images.get(s).getScaledInstance(scaleWidth, scaleHeight, Image.SCALE_SMOOTH));
        }
    }
    
    public static ArrayList<WeakReference<Tile>> getAllTiles(){ return allTiles; }
}