package utils.map;

import java.util.ArrayList;
import utils.debug.Out;
import utils.file.FileLoader;
import utils.procedural.ProceduralGeneration;

public class Map{
    public ArrayList<Tile> map = new ArrayList<Tile>();
    
    public Map(String file){
        String buffer = new String(FileLoader.readFile(file));
        String[] lines = buffer.split("\n");
        for(int y=0; y<lines.length; y++){
            String[] names = new String(lines[y]).split(",");
            for(int x=0; x<names.length; x++){
                Out.status(Out.State.DEBUG, "name -> " + names[x]);
                map.add(new Tile(x, y, names[x].trim()));
            }
        }
    }
    
    public Map(String key, int width, int height){
        //NOTE: Setup printable tiles
        Tile[] tiles = new Tile[]{new Tile(0, 0, "wall", new int[]{100, 150}, 180, 200),
                                  new Tile(0, 0, "water", new int[]{69, 102, 95}, 36, 39),
                                  new Tile(0, 0, "grass", new int[]{46546, 52543, 4543, 434, 453}, 54, 57)};
        //NOTE: Convert key
        char[] charArray = key.toCharArray();
        int[] keyArray = new int[charArray.length];
        for(int i=0; i<charArray.length; i++)keyArray[i] = charArray[i];
        
        //NOTE: Generate Map
        int[][] rawMap = ProceduralGeneration.generate(keyArray, width, height, 0, tiles.length - 1);
        
        //NOTE: Cluster water
        for(int y=2; y<rawMap.length - 2; y++){
            for(int x=2; x<rawMap[y].length - 2; x++){
                if(rawMap[y][x]==1){
                    if(rawMap[y][x+2]==1)rawMap[y][x+1] = 1;
                    if(rawMap[y+1][x]==1)rawMap[y+1][x] = 1;
                    if(rawMap[y][x+1]!=1||rawMap[y+1][x]!=1)rawMap[x][y] = 2;
                }
            }
        }
        
        //NOTE: Make sure map is playable
        for(int y=1; y<rawMap.length - 1; y++){
            for(int x=1; x<rawMap[y].length - 1; x++){
                if(rawMap[y][x+1]!=2&&rawMap[y][x-1]!=2||
                   rawMap[y+1][x]!=2&&rawMap[y-1][x]!=2||
                   rawMap[y+1][x+1]!=2&&rawMap[y-1][x-1]!=2||
                   rawMap[y+1][x+1]!=2&&rawMap[y-1][x-1]!=2){
                    rawMap[y][x+1] = 2;
                    rawMap[y+1][x] = 2;
                    rawMap[y+1][x+1] = 2;
                    rawMap[y][x] = 2;
                }
            }
        }
        
        //NOTE: Convert values to tiles
        for(int y=0; y<rawMap.length; y++){
            for(int x=0; x<rawMap[y].length; x++){
                map.add(new Tile(x, y, tiles[rawMap[x][y]]));
            }
        }
    }
}