package utils.settings;

import game.Game;
import java.io.InputStream;
import json.JSONArray;
import json.JSONObject;
import json.JSONTokener;

public class JsonHandler{
    private JSONObject jObj;
    
    public JsonHandler(String filename){
        InputStream is = Game.class.getResourceAsStream("/utils/settings/" + filename);
        JSONTokener jtoke = new JSONTokener(is);
        jObj = new JSONObject(jtoke);
    }
    
    public Object get(String key){ return jObj.get(key); }
    
    public boolean getBoolean(String key){ return jObj.getBoolean(key); }
    
    public double getDouble(String key){ return jObj.getDouble(key); }
    
    public int getInt(String key){ return jObj.getInt(key); }
    
    public JSONArray getJSONArray(String key){ return jObj.getJSONArray(key); }
    
    public JSONObject getJSONObject(String key){ return jObj.getJSONObject(key); }
    
    public long getLong(String key){ return jObj.getLong(key); }
    
    public String getString(String key){ return jObj.getString(key); }
}