package utils.debug;

public abstract class Out{
    public enum State{
        OK("[ OK ] "), FAIL("[FAIL] "), DEBUG("[DEBG] "), ERR("[ERRR] ");
        
        private String returnMsg;
        
        State(String returnMsg){ this.returnMsg = returnMsg; }
        
        String getReturnMsg(){ return returnMsg; }
    }
    
    public static void print(String msg){ System.out.print(msg); }
    
    public static void println(String msg){ print(msg + System.lineSeparator()); }
    
    public static void status(State state, String msg){ println(state.getReturnMsg() + msg); }
}